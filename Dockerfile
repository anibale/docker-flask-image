FROM alpine:latest
MAINTAINER Anibale <anibaleproducions@gmail.com>
RUN apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    rm -r /root/.cache
COPY ./requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt
RUN mkdir -p /opt/app
COPY ./app /opt/app/
WORKDIR /opt/app
EXPOSE 5000
CMD ["python3", "app.py"]/